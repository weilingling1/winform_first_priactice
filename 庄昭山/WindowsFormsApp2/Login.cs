﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("登入成功");
        
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
           var pd = MessageBox.Show("是否确前往注册！", "注册界面！",MessageBoxButtons.YesNo,MessageBoxIcon.Information);
            if (pd ==DialogResult.Yes)
            {
                var enroll = new Enroll();
                enroll.Show();
            }
            this.Hide();
        }
    }
}
