﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        
        private void btnRegister_Click(object sender, EventArgs e)
        {
            string name = this.txtName.Text;

            string pwd = this.txtPwd.Text;
           
            string info = name + "\n" + pwd;

            frmShow frmShow = new frmShow();

            frmShow.Show();

            frmShow.SetInfo(info);
            
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult dr = MessageBox.Show("你确定要取消登录吗？", "确认取消", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes)
            {
                Application.Exit();
            }
            else
            {
                this.txtName.Text = "";
                this.txtPwd.Text = "";
            }
        }
    }
}
